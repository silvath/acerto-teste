﻿using System;
using System.IO;
using System.Net.Http;
using MarvelAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Xunit;

namespace MarvelAPI.Tests
{

    public class CharacterControllerTest
    {
        CharacterController _controller = new CharacterController();
        public IConfiguration appSettings;

        public CharacterControllerTest()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(AppContext.BaseDirectory)
           .AddJsonFile($"appSettingsTest.json");

            appSettings = builder.Build();
        }

        [Fact]
        /* Testa a chamada da API da Marvel
         */
        public void IndexTestOK()
        {
            try
            {
                string characterTest = appSettings.GetSection("MarvelAPI:CharacterOkTest").Value;

                var result = _controller.Index(characterTest);
                Assert.IsType<ViewResult>(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Fact]
        /* Testa a chamada da API da Marvel em caso de nome errado do character
         */
        public void IndexTestError()
        {
            try
            {
                string characterTest = appSettings.GetSection("MarvelAPI:CharacterErrorTest").Value;

                var result = _controller.Index(characterTest);
                Assert.IsType<NotFoundObjectResult>(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Fact]
        /* Testa a chamada da geração do hash para passar como 
         * parametro na chamada da API
         */
        public void GenerateHashTestOK()
        {
            try
            {
                string ticket = appSettings.GetSection("MarvelAPI:Ticket").Value;
                string publicKey = appSettings.GetSection("MarvelAPI:PublicKey").Value;
                string privateKey = appSettings.GetSection("MarvelAPI:PrivateKey").Value;
                string hashExpected = appSettings.GetSection("MarvelAPI:HashExpected").Value;

                var result = _controller.GenerateHash(ticket, publicKey, privateKey);


                Assert.Equal(hashExpected, result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Fact]
        /* Testa a chamada da errada geração do hash para passar como 
         * parametro na chamada da API
         */
        public void GenerateHashTestError()
        {
            try
            {
                string ticket = appSettings.GetSection("MarvelAPI:TicketError").Value;
                string publicKey = appSettings.GetSection("MarvelAPI:PublicKey").Value;
                string privateKey = appSettings.GetSection("MarvelAPI:PrivateKey").Value;
                string hashExpectedError = appSettings.GetSection("MarvelAPI:HashExpectedError").Value;

                var result = _controller.GenerateHash(ticket, publicKey, privateKey);

                Assert.Equal(hashExpectedError, result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        [Fact]
        /* Testa a chamada da geração do log que grava 
         * os resultados das pesquisas. 
         */
        public void GenerateLogTestOk()
        {
            try
            {
                string pahtTest = appSettings.GetSection("MarvelAPI:FileLog").Value;
                string marvelCharacter = appSettings.GetSection("MarvelAPI:CharacterTest").Value;

                HttpClient client = new HttpClient();
                string publicKey = appSettings.GetSection("MarvelAPI:PublicKey").Value;
                string ticket = DateTime.Now.Ticks.ToString();
                string hash = _controller.GenerateHash(ticket, publicKey,
                        appSettings.GetSection("MarvelAPI:PrivateKey").Value);

                HttpResponseMessage response = client.GetAsync(appSettings.GetSection("MarvelAPI:URL").Value +
                            $"characters?ts={ticket}&apikey={publicKey}&hash={hash}&" +
                            $"name={Uri.EscapeUriString(marvelCharacter)}").Result;

                string content = response.Content.ReadAsStringAsync().Result;

                dynamic result = JsonConvert.DeserializeObject(content);
                var resultMethod = _controller.GenerateLog(result);

                Assert.Equal(resultMethod, pahtTest);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
