﻿using Marvel.API.Controllers;
using MarvelAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MarvelAPI.Tests
{
    public class HomeControllerTest
    {
        HomeController _controller = new HomeController();
        public IConfiguration appSettings;

        public HomeControllerTest()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(AppContext.BaseDirectory)
           .AddJsonFile($"appSettingsTest.json");

            appSettings = builder.Build();
        }

        [Fact]
        /* Testa a chamada Home da API da Marvel
         */
        public void IndexTestOK()
        {
            try
            {
                var result = _controller.Index();

                Assert.IsType<ViewResult>(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
