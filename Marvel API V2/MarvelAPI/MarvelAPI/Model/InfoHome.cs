﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvel.API.Model
{
    public class InfoHome
    {
        public string Link { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Examples { get; set; }

    }
}
