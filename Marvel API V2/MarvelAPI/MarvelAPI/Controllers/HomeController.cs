﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marvel.API.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Marvel.API.Controllers
{
    [Route("api/[controller]")]
    public class HomeController : Controller
    {
        public IConfiguration appSettings;

        public HomeController()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            appSettings = builder.Build();
        }

        public IActionResult Index()
        {
            string error = appSettings.GetSection("MarvelAPI:Error").Value;
            try
            {
                InfoHome infoHome = new InfoHome();
                infoHome.Title = appSettings.GetSection("MarvelAPI:Title").Value;
                infoHome.Description = appSettings.GetSection("MarvelAPI:Description").Value;
                infoHome.Link = appSettings.GetSection("MarvelAPI:Link").Value;
                infoHome.Examples = appSettings.GetSection("MarvelAPI:Examples").Value;

                return View(infoHome);
            }
            catch (Exception ex)
            {
                return NotFound(error + ex);
            }
        
        }
    }
}