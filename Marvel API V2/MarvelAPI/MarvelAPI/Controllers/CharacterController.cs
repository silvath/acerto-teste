﻿using System;
using System.Net.Http;
using Marvel.API.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using System.IO;


namespace MarvelAPI.Controllers
{
    [Route("api/[controller]")]

    [ApiController]
    public class CharacterController : Controller
    {

        public IConfiguration appSettings;

        public CharacterController()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            appSettings = builder.Build();
        }

        [HttpGet(Name ="event")]
        public IActionResult Index(string marvelCharacter)
        {
            Character character = new Character();
            string content = "";
            HttpClient client = new HttpClient();
            string publicKey = appSettings.GetSection("MarvelAPI:PublicKey").Value;
            string ticket = DateTime.Now.Ticks.ToString();
            string hash = GenerateHash(ticket, publicKey, appSettings.GetSection("MarvelAPI:PrivateKey").Value);
            string error = appSettings.GetSection("MarvelAPI:Error").Value;

            try
                {
                    HttpResponseMessage response = client.GetAsync(appSettings.GetSection("MarvelAPI:URL").Value +
                        $"characters?ts={ticket}&apikey={publicKey}&hash={hash}&" +
                        $"name={Uri.EscapeUriString(marvelCharacter)}").Result;

                     content = response.Content.ReadAsStringAsync().Result;

                    dynamic result = JsonConvert.DeserializeObject(content);

                    character.Name = result.data.results[0].name;
                    character.ID = result.data.results[0].id;
                    character.Description = result.data.results[0].description;
                    character.LinkPhoto = result.data.results[0].thumbnail.path + "." + result.data.results[0].thumbnail.extension;

                    for (int i = 0; i < Convert.ToInt32(result.data.results[0].stories.returned); i++)
                    {
                        character.Story += "Name: " + result.data.results[0].stories.items[i].name + " \\ ";
                    }

                    GenerateLog(result);
                }
                catch (Exception ex)
                {
                    return NotFound(error + ex);
                }
            return View(character);
        }

        /* Geração do log que grava 
         * os resultados das pesquisas.
         */
        public string GenerateLog(dynamic json)
        {
            string name;
            int id;
            string description;
            string story = " ";
            DateTime date = DateTime.Now;

            try
            {

                name = json.data.results[0].name;
                id = json.data.results[0].id;
                description = json.data.results[0].description;
                for (int i = 0; i < Convert.ToInt32(json.data.results[0].stories.returned); i++)
                {
                    story += "Name: " + json.data.results[0].stories.items[i].name + " \\ ";
                }

                string path = appSettings.GetSection("MarvelAPI:FileLog").Value;
                string folderPath = appSettings.GetSection("MarvelAPI:FolderPath").Value;
                FileInfo file = new FileInfo(path);
                if (!file.Exists || !Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);

                    using (FileStream fs = System.IO.File.Create(path))
                    {
                        Byte[] info = new UTF8Encoding(true).GetBytes("Log");
                        fs.Close();
                    }
                }
                using (var txt = new StreamWriter(path, true))
                {
                    string line = "Date: " + date + "\r\n" + "Name:" + name + "\r\n" + "ID:"
                                        + id + "\r\n" + "Description: " + description + "\r\n" + "Story: " + story + "\r\n" + "-----------";
                    txt.WriteLine(line);
                    txt.Close();
                }
                return path;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /* Geração do hash para passar como 
         * parametro na chamada da API
         */
        public string GenerateHash(string ticket, string publicKey, string privateKey)
        {
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(ticket + privateKey + publicKey);
                var md5 = MD5.Create();
                byte[] hash = md5.ComputeHash(bytes);

                return BitConverter.ToString(hash).ToLower().Replace("-", String.Empty);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }
    }
}