﻿using System;
using System.Net.Http;
using MarvelAPI.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using System.IO;


namespace MarvelAPI.Controllers
{
    [Route("api/[controller]")]

    [ApiController]
    public class CharacterController : Controller
    {

        public IConfiguration appSettings;

        [HttpGet(Name = "event")] 
        /* Chamada da API da Marvel
         */
        public IActionResult Index()
        {
            Character character = new Character();

            /* Coloque o nome do personagem 
             * na string abaixo (marvelCharacter)
             * Exemplos para pesquisa
             * -Iron Man
             * -Hulk
             * -Spider-Man
             */ 
            string marvelCharacter = "Iron Man";

            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            appSettings = builder.Build();

            var client = new HttpClient();
            string publicKey = appSettings.GetSection("MarvelAPI:PublicKey").Value;
            string ticket = DateTime.Now.Ticks.ToString();
            string hash = GenerateHash(ticket, publicKey,
                    appSettings.GetSection("MarvelAPI:PrivateKey").Value);

            HttpResponseMessage response = client.GetAsync(appSettings.GetSection("MarvelAPI:URL").Value +
                    $"characters?ts={ticket}&apikey={publicKey}&hash={hash}&" +
                    $"name={Uri.EscapeUriString(marvelCharacter)}").Result;

            string content = response.Content.ReadAsStringAsync().Result;
            
            dynamic result = JsonConvert.DeserializeObject(content);

            character.Name = result.data.results[0].name;
            character.ID = result.data.results[0].id;
            character.Description = result.data.results[0].description;
            character.LinkPhoto = result.data.results[0].thumbnail.path + "." + result.data.results[0].thumbnail.extension;

            for (int i = 0; i < Convert.ToInt32(result.data.results[0].stories.returned); i++)
            {
                character.Story += "Name: " + result.data.results[0].stories.items[i].name + " \\ ";
            }

            GenerateLog(result);
            return View(character);
        }

        /* Geração do log que grava 
         * os resultados das pesquisas.
         */
        public string GenerateLog(dynamic json)
        {
            string name;
            int id;
            string description;
            string story = " ";
            DateTime date = DateTime.Now;

            name = json.data.results[0].name;
            id = json.data.results[0].id;
            description = json.data.results[0].description;
            for (int i = 0; i < Convert.ToInt32(json.data.results[0].stories.returned); i++)
            {
                story += "Name: " + json.data.results[0].stories.items[i].name + " \\ ";
            }

            string path =  @"C:\\Log\\LogMarvelAPI.txt"; //appSettings.GetSection("MarvelAPI:FileLog").Value; 
            string folderPath = @"C:\\Log\\"; //appSettings.GetSection("MarvelAPI:FolderPath").Value; 
            FileInfo file = new FileInfo(path);
            if (!file.Exists || !Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
                
                using (FileStream fs = System.IO.File.Create(path))
                {
                    Byte[] info = new UTF8Encoding(true).GetBytes("Log");
                    fs.Flush();
                    fs.Close();
                    fs.Dispose();
                    
                }
            }
            using (var txt = new StreamWriter(path, true))
            {
                string line = "Date: " + date + "\r\n" + "Name:" + name + "\r\n" + "ID:"
                                    + id + "\r\n" + "Description: " + description + "\r\n" + "Story: " + story + "\r\n" + "-----------";
                txt.WriteLine(line);
                txt.Flush();
                txt.Close();
                txt.Dispose();
            }
            return path;
        }

        /* Geração do hash para passar como 
         * parametro na chamada da API
         */
        public string GenerateHash(
           string hour, string publicKey, string privateKey)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(hour + privateKey + publicKey);
            var md5 = MD5.Create();
            byte[] hash = md5.ComputeHash(bytes);

            return BitConverter.ToString(hash).ToLower().Replace("-", String.Empty);
        }
    }
}