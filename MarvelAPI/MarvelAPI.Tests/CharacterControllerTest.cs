﻿using System;
using System.IO;
using System.Net.Http;
using MarvelAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Xunit;

namespace MarvelAPI.Tests
{

    public class CharacterControllerTest
    {
        CharacterController _controller = new CharacterController();
        public IConfiguration appSettings;

        [Fact]
        /* Testa a chamada da API da Marvel
         */
        public void IndexTest()
        {
            var result = _controller.Index();
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        /* Testa a chamada da geração do hash para passar como 
         * parametro na chamada da API
         */
        public void GenerateHashTest()
        {
            var result = _controller.GenerateHash("636862973664743305", "29d645c3c12c3292fb2507be2647e668",
                                                "73af1b57111dab2e307ac9f0427576271f5dea85");

            Assert.Equal("e6feec152169c6e20f78b20da9a02af7", result);
        }

        [Fact]
        /* Testa a chamada da geração da geração do log que grava 
         * os resultados das pesquisas. 
         */
        public void GenerateLogTest() 
        {

            string pahtTest = @"C:\\Log\\LogMarvelAPI.txt";
            string marvelCharacter = "Iron Man";

            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            appSettings = builder.Build();

            var client = new HttpClient();
            string publicKey = appSettings.GetSection("MarvelAPI:PublicKey").Value;
            string hour = DateTime.Now.Ticks.ToString();
            string hash = _controller.GenerateHash(hour, publicKey,
                    appSettings.GetSection("MarvelAPI:PrivateKey").Value);

            HttpResponseMessage response = client.GetAsync(appSettings.GetSection("MarvelAPI:URL").Value +
                    $"characters?ts={hour}&apikey={publicKey}&hash={hash}&" +
                    $"name={Uri.EscapeUriString(marvelCharacter)}").Result;

            string content = response.Content.ReadAsStringAsync().Result;

            dynamic result = JsonConvert.DeserializeObject(content);
            var resultMethod = _controller.GenerateLog(result);

            Assert.Equal(resultMethod, pahtTest);
        }
    }
}
